package com.nowak.calculator;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {

    @GetMapping("/")
    public String calculate() {
        return "calculator";
    }

    @PostMapping("/data")
    public String value(@ModelAttribute Calculator bmi, ModelMap map) {

        map.put("warningMass", bmi.isMassCorrect());
        map.put("warningHeight", bmi.isHeightCorrect());
        map.put("bmi", bmi.printBmi());
        map.put("description", bmi.printDescription());

        return "calculator";
    }
}
