package com.nowak.calculator;


import org.junit.Assert;
import org.junit.Test;


public class CalculatorTest {

    @Test
    public void TestGetBmi() throws Exception {
        Calculator calculator = new Calculator(1.0,1.0);
        Assert.assertEquals(calculator.getBmi(), 1, 0);
        calculator = new Calculator(85.0,1.80);
        Assert.assertEquals(calculator.getBmi(), 26.2, 0.06);
        calculator = new Calculator(null,null);
        Assert.assertEquals(calculator.getBmi(), 0.0, 0.00);
    }

    @Test
    public void TestIsMassCorrect() throws Exception {
        Calculator calculator = new Calculator(1.0,1.0);
        Assert.assertEquals(calculator.isMassCorrect(), "Podaj prawidłową wagę!");
        calculator = new Calculator(85.0,1.80);
        Assert.assertEquals(calculator.isMassCorrect(), "");
        calculator = new Calculator(null,null);
        Assert.assertEquals(calculator.isMassCorrect(), "Podaj wagę!");
    }

    @Test
    public void TestIsHeightCorrect() throws Exception {
        Calculator calculator = new Calculator(1.0,1.0);
        Assert.assertEquals(calculator.isHeightCorrect(), "Podaj prawidłowy wzrost!");
        calculator = new Calculator(85.0,1.80);
        Assert.assertEquals(calculator.isHeightCorrect(), "");
        calculator = new Calculator(null,null);
        Assert.assertEquals(calculator.isHeightCorrect(), "Podaj wzrost!");
    }

    @Test
    public void TestCheck() throws Exception {
        Calculator calculator = new Calculator(2.0, 1.0);
        Assert.assertFalse(calculator.check());
        calculator = new Calculator(85.0,1.80);
        Assert.assertTrue(calculator.check());
        calculator = new Calculator(null,null);
        Assert.assertFalse(calculator.check());
    }

    @Test
    public void TestPrintBmi() throws Exception {
        Calculator calculator = new Calculator(2.0, 1.0);
        Assert.assertEquals(calculator.printBmi(), "");
        calculator = new Calculator(85.0,1.80);
        Assert.assertEquals(calculator.printBmi(), "Twój wskaźnik BMI to: 26,2");
        calculator = new Calculator(null,null);
        Assert.assertEquals(calculator.printBmi(), "");
    }

    @Test
    public void TestPrintDescriptioni() throws Exception {
        Calculator calculator = new Calculator(2.0, 1.0);
        Assert.assertEquals(calculator.printDescription(), "");
        calculator = new Calculator(85.0,1.80);
        Assert.assertEquals(calculator.printDescription(), "Nadwaga.");
        calculator = new Calculator(null,null);
        Assert.assertEquals(calculator.printDescription(), "");
    }
}
