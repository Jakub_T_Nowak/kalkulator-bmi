package com.nowak.calculator;

public class Calculator {
    private Double mass;
    private Double height;
    private Double bmi;

    public Calculator(Double mass, Double height) {
        setMass(mass);
        setHeight(height);
        setBmi();
    }

    public void setMass(Double mass) {
        if (mass != null) {
            this.mass = mass;
        } else {
            this.mass = 0.0;
        }
    }

    public void setHeight(Double height) {
        if (height != null) {
            this.height = height;
        } else {
            this.height = 0.0;
        }
    }

    public void setBmi() {
        if (mass != 0 && height != 0) {
            this.bmi = mass / (height * height);
        } else {
            this.bmi = 0.0;
        }
    }


    public Double getBmi() {
        return bmi;
    }

    public String isMassCorrect() {
        if (mass == 0.0) {
            return "Podaj wagę!";
        } else if (mass < 40 || mass > 180) {
            return "Podaj prawidłową wagę!";
        }
        return "";
    }

    public String isHeightCorrect() {
        if (height == 0.0) {
            return "Podaj wzrost!";
        } else if (height < 1.4 || height > 2.4) {
            return "Podaj prawidłowy wzrost!";
        }
        return "";
    }

    public boolean check() {
        if (mass >= 40 && mass <= 180 && height >= 1.4 && height <= 2.4) {
            return true;
        } else
            return false;
    }

    public String printBmi() {
        if (check()) {
            return "Twój wskaźnik BMI to: " + String.format("%.1f", bmi);
        } else {
            return "";
        }
    }

    public String printDescription() {
        if (check()) {
            if (bmi < 18.5) {
                return "Niedowaga.";
            } else if (bmi > 25) {
                return "Nadwaga.";
            } else {
                return "Masz prawidłową wagę.";
            }
        } else {
            return "";
        }
    }
}